/************************************************
UNIT TESTS for TubeBarcodeChecker.js
*************************************************/

/************************************************
UNIT TEST for TubeBarcodeChecker.resetState()
sets all mutable properties of object, then calls resetState() 
full test results output to VWorks log
RETURNS: BOOLEAN (true if test passes; false if test fails)
*************************************************/
function unit_test_resetState() {
	print("INITIAL STATE:")
	TubeBarcodeChecker.resetState()
	print(TubeBarcodeChecker.printState())
	print("------------------")

	print("MIDDLE STATE:")
	TubeBarcodeChecker.rackBarcode = "12345678"
	TubeBarcodeChecker.fileName = "rack_12345678.csv"
	TubeBarcodeChecker.badBarcodeFlag = true
	TubeBarcodeChecker.badInputFlag = true
	TubeBarcodeChecker.lineArray = ["abcd","1234"]
	TubeBarcodeChecker.missingLoc = "A1"
	TubeBarcodeChecker.missingLineNum = 1
	TubeBarcodeChecker.titleText = "MISSING BARCODE AT TUBE: A1"
	TubeBarcodeChecker.newTubeBarcode = "87654321"
	print(TubeBarcodeChecker.printState())
	print("------------------")

	print("FINAL STATE:")
	TubeBarcodeChecker.resetState()
	print(TubeBarcodeChecker.printState())
	print("------------------")

	if (TubeBarcodeChecker.rackBarcode 		== null &&
		TubeBarcodeChecker.fileName 		== null &&
		TubeBarcodeChecker.badBarcodeFlag 	== null &&
		TubeBarcodeChecker.badInputFlag 	== null &&
		TubeBarcodeChecker.lineArray 	   	== null &&
		TubeBarcodeChecker.missingLoc 	   	== null &&
		TubeBarcodeChecker.missingLineNum 	== null &&
		TubeBarcodeChecker.titleText 	   	== null &&
		TubeBarcodeChecker.newTubeBarcode 	== null)
		{print("unit_test_resetState(): PASS"); return true}
	else {print("unit_test_resetState(): FAIL"); return false}
}

/************************************************
UNIT TEST for TubeBarcodeChecker.setRackBarcode()
tests setting different values for rack barcode
full test results output to VWorks log
RETURNS: BOOLEAN (true if test passes; false if test fails)
*************************************************/
function unit_test_setRackBarcode() {

	var result = true

	TubeBarcodeChecker.resetState()
	var rackBarcode = null
	print("testing function argument rackBarcode: " + rackBarcode)
	TubeBarcodeChecker.setRackBarcode(rackBarcode)
	if (TubeBarcodeChecker.rackBarcode == null) {print("result: pass")}
	else {print("result: fail"); result = false}
 
 	TubeBarcodeChecker.resetState()
 	var rackBarcode = undefined
	print("testing function argument rackBarcode: " + rackBarcode)
	TubeBarcodeChecker.setRackBarcode(rackBarcode)
	if (TubeBarcodeChecker.rackBarcode == null) {print("result: pass")} 
	else {print("result: fail"); result = false}

	TubeBarcodeChecker.resetState()
 	var rackBarcode = "12345678"
	print("testing function argument rackBarcode: " + rackBarcode)
	TubeBarcodeChecker.setRackBarcode(rackBarcode)
	if (TubeBarcodeChecker.rackBarcode == rackBarcode) {print("result: pass")} 
	else {print("result: fail"); result = false}

	TubeBarcodeChecker.resetState()
 	var rackBarcode = "01234567"
	print("testing function argument rackBarcode: " + rackBarcode)
	TubeBarcodeChecker.setRackBarcode(rackBarcode)
	if (TubeBarcodeChecker.rackBarcode == rackBarcode) {print("result: pass")} 
	else {print("result: fail"); result = false}

	TubeBarcodeChecker.resetState()
 	var rackBarcode = "1D123456"
	print("testing function argument rackBarcode: " + rackBarcode)
	TubeBarcodeChecker.setRackBarcode(rackBarcode)
	if (TubeBarcodeChecker.rackBarcode == rackBarcode) {print("result: pass")} 
	else {print("result: fail"); result = false}

	TubeBarcodeChecker.resetState()
 	var rackBarcode = "R1234567"
	print("testing function argument rackBarcode: " + rackBarcode)
	TubeBarcodeChecker.setRackBarcode(rackBarcode)
	if (TubeBarcodeChecker.rackBarcode == rackBarcode) {print("result: pass")} 
	else {print("result: fail"); result = false}

	if (result) {print("unit_test_setRackBarcode(): PASS"); return true}
	else {print("unit_test_setRackBarcode(): FAIL"); return false}
}

/************************************************
UNIT TEST for TubeBarcodeChecker.findLatestFile()
tests different folder contents to see if correct file can be located
full test results output to VWorks log
RETURNS: BOOLEAN (true if test passes; false if test fails)
*************************************************/
function unit_test_findLatestFile() {
	FOLDER_AUTOMATED_TESTS = ROOT_FOLDER_AUTOMATED_TESTS + "unit tests for findLatestFile()\\"
	
	var result = true

	print("UNIT TESTS for TubeBarcodeChecker.readFile()")
	print("root folder for tests: " + FOLDER_AUTOMATED_TESTS)

	// test if rackBarcode = null
	var folder_name = "test1\\"
	print("testing folder: " + folder_name)
	print("test if rackBarcode = null")
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.findLatestFile()
	if (TubeBarcodeChecker.fileName == null) {print("result: pass")}
	else {print("result: fail"); result = false}

	// test just one file
	var folder_name = "test1\\"
	print("testing folder: " + folder_name)
	print("test just one file")
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678") // same rack barcode used for all unit tests here
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.findLatestFile()
	if (TubeBarcodeChecker.fileName == "rack_12345678.csv") {print("result: pass")}
	else {print("result: fail"); result = false}

	// test two files: "...csv" and "..._1.csv"
	var folder_name = "test2\\"
	print("testing folder: " + folder_name)
	print("test two files: \"...csv\" and \"..._1.csv\"") 
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678") // same rack barcode used for all unit tests here
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.findLatestFile()
	if (TubeBarcodeChecker.fileName == "rack_12345678_1.csv") {print("result: pass")}
	else {print("result: fail"); result = false}

	// test multiple files up to "..._10.csv"
	var folder_name = "test3\\"
	print("testing folder: " + folder_name)
	print("test multiple files up to \"..._10.csv\"")
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678") // same rack barcode used for all unit tests here
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	print("testing folder: " + folder_name)
	TubeBarcodeChecker.findLatestFile()
	if (TubeBarcodeChecker.fileName == "rack_12345678_10.csv") {print("result: pass")}
	else {print("result: fail"); result = false}

	// test empty folder
	var folder_name = "test4\\"
	print("testing folder: " + folder_name)
	print("test empty folder")
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678") // same rack barcode used for all unit tests here
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.findLatestFile()
	if (TubeBarcodeChecker.fileName == null) {print("result: pass")}
	else {print("result: fail"); result = false}

	// test folder with another tube barcode file present
	var folder_name = "test5\\"
	print("testing folder: " + folder_name)
	print("test folder when another tube barcode file is present")
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678") // same rack barcode used for all unit tests here
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.findLatestFile()
	if (TubeBarcodeChecker.fileName == "rack_12345678.csv") {print("result: pass")}
	else {print("result: fail"); result = false}

	// test folder with only a NOREAD0 file present
	var folder_name = "test6\\"
	print("testing folder: " + folder_name)
	print("test folder with only a NOREAD0 file present")
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678") // same rack barcode used for all unit tests here
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.findLatestFile()
	if (TubeBarcodeChecker.fileName == null) {print("result: pass")}
	else {print("result: fail"); result = false}

	// RESET TO DEFAULT!
	TubeBarcodeChecker.FOLDER_NAME = "C:\\vworks workspace\\Barcode Files\\"

	if (result) {print("unit_test_findLatestFile(): PASS"); return true}
	else {print("unit_test_findLatestFile(): FAIL"); return false}
}

/************************************************
UNIT TEST for TubeBarcodeChecker.findLatestFile()
tests different folder contents to see if correct file can be located
full test results output to VWorks log
RETURNS: BOOLEAN (true if test passes; false if test fails)
*************************************************/
function unit_test_readFile() {
	FOLDER_AUTOMATED_TESTS = ROOT_FOLDER_AUTOMATED_TESTS + "unit tests for readFile()\\"
	
	var result = true

	print("UNIT TESTS for TubeBarcodeChecker.readFile()")
	print("root folder for tests: " + FOLDER_AUTOMATED_TESTS)

	// test to make sure file is not altered by readFile()
	var folder_name = "test1\\"
	print("testing folder: " + folder_name)
	print("make sure CSV file is not altered by readFile")
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678") // same rack barcode used for all unit tests here
	TubeBarcodeChecker.findLatestFile()
	
	// BEFORE readFile()
	var path1 = FOLDER_AUTOMATED_TESTS + folder_name + "rack_12345678.csv"
	var file1 = new File()
	file1.Open(path1, 0, 0)
	var file_contents1 = file1.Read()
	file1.Close()

	TubeBarcodeChecker.readFile()

	// AFTER readFile()
	var path2 = FOLDER_AUTOMATED_TESTS + folder_name + "rack_12345678.csv"
	var file2 = new File()
	file2.Open(path2, 0, 0)
	var file_contents2 = file2.Read()
	file2.Close()

	if (file_contents1 == file_contents2) {print("result: pass")}
	else {print("result: fail"); result = false}

	// test null barcode
	var folder_name = "test1\\"
	print("testing folder: " + folder_name)
	print("test if rackBarcode=null")
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode(null) // same rack barcode used for all unit tests here
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	if (TubeBarcodeChecker.badBarcodeFlag 	== null &&
		TubeBarcodeChecker.missingLoc 		== null &&
		TubeBarcodeChecker.missingLineNum 	== null) 
		{print("result: pass")}
	else {print("result: fail"); result = false}

	// test file does not exist
	var folder_name = "test1\\"
	print("testing folder: " + folder_name)
	print("test if file does not exist")
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("01234567") // same rack barcode used for all unit tests here
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	if (TubeBarcodeChecker.badBarcodeFlag 	== null &&
		TubeBarcodeChecker.missingLoc 		== null &&
		TubeBarcodeChecker.missingLineNum 	== null) 
		{print("result: pass")}
	else {print("result: fail"); result = false}

	// test good file
	var folder_name = "test1\\"
	print("testing folder: " + folder_name)
	print("test file with 96 correct barcodes")
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678") // same rack barcode used for all unit tests here
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	if (TubeBarcodeChecker.badBarcodeFlag 	== false &&
		TubeBarcodeChecker.missingLoc 		== null &&
		TubeBarcodeChecker.missingLineNum 	== null) 
		{print("result: pass")}
	else {print("result: fail"); result = false}

	// test file with EMPTY in A1
	var folder_name = "test2\\"
	print("testing folder: " + folder_name)
	print("test file with EMPTY in A1")
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678") // same rack barcode used for all unit tests here
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	if (TubeBarcodeChecker.badBarcodeFlag 	== true &&
		TubeBarcodeChecker.missingLoc 		== "A1" &&
		TubeBarcodeChecker.missingLineNum 	== 0) 
		{print("result: pass")}
	else {print("result: fail"); result = false}

	// test file with DECODE FAILURE in A1
	var folder_name = "test3\\"
	print("testing folder: " + folder_name)
	print("test file with DECODE FAILURE in A1")
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678") // same rack barcode used for all unit tests here
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	if (TubeBarcodeChecker.badBarcodeFlag 	== true &&
		TubeBarcodeChecker.missingLoc 		== "A1" &&
		TubeBarcodeChecker.missingLineNum 	== 0) 
		{print("result: pass")}
	else {print("result: fail"); result = false}

	// test file with "|" in barcode A1
	var folder_name = "test4\\"
	print("testing folder: " + folder_name)
	print("test file with | in barcode in A1")
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678") // same rack barcode used for all unit tests here
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	if (TubeBarcodeChecker.badBarcodeFlag 	== true &&
		TubeBarcodeChecker.missingLoc 		== "A1" &&
		TubeBarcodeChecker.missingLineNum 	== 0) 
		{print("result: pass")}
	else {print("result: fail"); result = false}

	// test file with short barcode in A1
	var folder_name = "test5\\"
	print("testing folder: " + folder_name)
	print("test file with 1234567 barcode (short) in A1")
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678") // same rack barcode used for all unit tests here
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	if (TubeBarcodeChecker.badBarcodeFlag 	== true &&
		TubeBarcodeChecker.missingLoc 		== "A1" &&
		TubeBarcodeChecker.missingLineNum 	== 0) 
		{print("result: pass")}
	else {print("result: fail"); result = false}

	// test file with white space in front of barcode in A1
	var folder_name = "test6\\"
	print("testing folder: " + folder_name)
	print("test file with \" 12345678\" barcode (leading whitespace) in A1")
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678") // same rack barcode used for all unit tests here
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	if (TubeBarcodeChecker.badBarcodeFlag 	== false &&
		TubeBarcodeChecker.missingLoc 		== null &&
		TubeBarcodeChecker.missingLineNum 	== null) 
		{print("result: pass")}
	else {print("result: fail"); result = false}

	// test file with EMPTY in H12
	var folder_name = "test7\\"
	print("testing folder: " + folder_name)
	print("test file with EMPTY in H12")
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678") // same rack barcode used for all unit tests here
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	if (TubeBarcodeChecker.badBarcodeFlag 	== true &&
		TubeBarcodeChecker.missingLoc 		== "H12" &&
		TubeBarcodeChecker.missingLineNum 	== 95) 
		{print("result: pass")}
	else {print("result: fail"); result = false}

	// test file with EMPTY in A2 and H12
	var folder_name = "test8\\"
	print("testing folder: " + folder_name)
	print("test file with EMPTY in A2 and H12")
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678") // same rack barcode used for all unit tests here
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	if (TubeBarcodeChecker.badBarcodeFlag 	== true &&
		TubeBarcodeChecker.missingLoc 		== "A2" &&
		TubeBarcodeChecker.missingLineNum 	== 1) 
		{print("result: pass")}
	else {print("result: fail"); result = false}

	// test file with "0909169" in A2
	var folder_name = "test9\\"
	print("testing folder: " + folder_name)
	print("test file with \"0909169\" in A2")
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678") // same rack barcode used for all unit tests here
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	if (TubeBarcodeChecker.badBarcodeFlag 	== true &&
		TubeBarcodeChecker.missingLoc 		== "A2" &&
		TubeBarcodeChecker.missingLineNum 	== 1) 
		{print("result: pass")}
	else {print("result: fail"); result = false}

	// test file with " 0909169" in A2
	var folder_name = "test10\\"
	print("testing folder: " + folder_name)
	print("test file with \" 0909169\" in A2")
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678") // same rack barcode used for all unit tests here
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	if (TubeBarcodeChecker.badBarcodeFlag 	== false) 
		{print("result: pass")}
	else {print("result: fail"); result = false}

	// test file with "P0000000" in A1
	var folder_name = "test11\\"
	print("testing folder: " + folder_name)
	print("test file with \" P0000000\" in A1")
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678") // same rack barcode used for all unit tests here
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	if (TubeBarcodeChecker.badBarcodeFlag 	== false) 
		{print("result: pass")}
	else {print("result: fail"); result = false}

	// test file with "Z0000000" in A1
	var folder_name = "test12\\"
	print("testing folder: " + folder_name)
	print("test file with \" Z0000000\" in A1")
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678") // same rack barcode used for all unit tests here
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	if (TubeBarcodeChecker.badBarcodeFlag 	== true &&
		TubeBarcodeChecker.missingLoc 		== "A1" &&
		TubeBarcodeChecker.missingLineNum 	== 0) 		
		{print("result: pass")}
	else {print("result: fail"); result = false}

	// RESET TO DEFAULT!
	TubeBarcodeChecker.FOLDER_NAME = "C:\\vworks workspace\\Barcode Files\\"

	if (result) {print("unit_test_readFile(): PASS"); return true}
	else {print("unit_test_readFile(): FAIL"); return false}
}

/************************************************
UNIT TEST for TubeBarcodeChecker.setNewBarcode()
tests different values for tube barcode
full test results output to VWorks log
RETURNS: BOOLEAN (true if test passes; false if test fails)
*************************************************/
function unit_test_setTubeBarcode() {
	var result = true

	print("UNIT TESTS for TubeBarcodeChecker.setTubeBarcode()")

	TubeBarcodeChecker.resetState()
	var tubeBarcode = null
	print("testing function argument tubeBarcode: " + tubeBarcode)
	TubeBarcodeChecker.setTubeBarcode(tubeBarcode)
	if (TubeBarcodeChecker.newTubeBarcode == null) {print("result: pass")}
	else {print("result: fail"); result = false}

	TubeBarcodeChecker.resetState()
	var tubeBarcode = undefined
	print("testing function argument tubeBarcode: " + tubeBarcode)
	TubeBarcodeChecker.setTubeBarcode(tubeBarcode)
	if (TubeBarcodeChecker.newTubeBarcode == null) {print("result: pass")}
	else {print("result: fail"); result = false}

	TubeBarcodeChecker.resetState()
	var tubeBarcode = "12345678"
	print("testing function argument tubeBarcode: " + tubeBarcode)
	TubeBarcodeChecker.setTubeBarcode(tubeBarcode)
	if (TubeBarcodeChecker.newTubeBarcode == tubeBarcode) {print("result: pass")}
	else {print("result: fail"); result = false}

	TubeBarcodeChecker.resetState()
	var tubeBarcode = "01234567"
	print("testing function argument tubeBarcode: " + tubeBarcode)
	TubeBarcodeChecker.setTubeBarcode(tubeBarcode)
	if (TubeBarcodeChecker.newTubeBarcode== tubeBarcode) {print("result: pass")}
	else {print("result: fail"); result = false}

	if (result) {print("unit_test_setTubeBarcode(): PASS"); return true}
	else {print("unit_test_setTubeBarcode(): FAIL"); return false}
}

/************************************************
UNIT TEST for TubeBarcodeChecker.resetTubeBarcode()
tests that tube barcode is always reset!
full test results output to VWorks log
RETURNS: BOOLEAN (true if test passes; false if test fails)
*************************************************/
function unit_test_resetTubeBarcode() {
	var result = true

	print("UNIT TESTS for TubeBarcodeChecker.resetTubeBarcode()")

	TubeBarcodeChecker.resetState()
	var tubeBarcode = null
	print("testing after newTubeBarcode is set to: " + tubeBarcode)
	TubeBarcodeChecker.setTubeBarcode(tubeBarcode)
	TubeBarcodeChecker.resetTubeBarcode()
	if (TubeBarcodeChecker.newTubeBarcode == null) {print("result: pass")}
	else {print("result: fail"); result = false}

	TubeBarcodeChecker.resetState()
	var tubeBarcode = undefined
	print("testing after newTubeBarcode is set to: " + tubeBarcode)
	TubeBarcodeChecker.setTubeBarcode(tubeBarcode)
	TubeBarcodeChecker.resetTubeBarcode()
	if (TubeBarcodeChecker.newTubeBarcode == null) {print("result: pass")}
	else {print("result: fail"); result = false}

	TubeBarcodeChecker.resetState()
	var tubeBarcode = "12345678"
	print("testing after newTubeBarcode is set to: " + tubeBarcode)
	TubeBarcodeChecker.setTubeBarcode(tubeBarcode)
	TubeBarcodeChecker.resetTubeBarcode()
	if (TubeBarcodeChecker.newTubeBarcode == null) {print("result: pass")}
	else {print("result: fail"); result = false}
	
	

	if (result) {print("unit_test_resetTubeBarcode(): PASS"); return true}
	else {print("unit_test_resetTubeBarcode(): FAIL"); return false}
}

/************************************************
UNIT TEST for TubeBarcodeChecker.checkInput()
tests that checking user inputted newBarcode is valid and not a duplicate
full test results output to VWorks log
RETURNS: BOOLEAN (true if test passes; false if test fails)
*************************************************/
function unit_test_checkInput() {
	var result = true

	FOLDER_AUTOMATED_TESTS = ROOT_FOLDER_AUTOMATED_TESTS + "unit tests for checkInput()\\"
	
	print("UNIT TESTS for TubeBarcodeChecker.checkInput()")
	print("root folder for tests: " + FOLDER_AUTOMATED_TESTS)

	var folder_name = "test1\\"
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	print("testing folder: " + folder_name)
	print("check if newTubeBarcode is NOT set")
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.checkInput()
	if (TubeBarcodeChecker.badInputFlag == true) {print("result: pass")}
	else {print("result: fail"); result = false}

	var folder_name = "test1\\"
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	print("testing folder: " + folder_name)
	print("check if newTubeBarcode is set to null")
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setTubeBarcode(null)
	TubeBarcodeChecker.checkInput()
	if (TubeBarcodeChecker.badInputFlag == true) {print("result: pass")}
	else {print("result: fail"); result = false}

	var folder_name = "test1\\"
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	print("testing folder: " + folder_name)
	print("check if newTubeBarcode is set to undefined")
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setTubeBarcode(undefined)
	TubeBarcodeChecker.checkInput()
	if (TubeBarcodeChecker.badInputFlag == true) {print("result: pass")}
	else {print("result: fail"); result = false}

	var folder_name = "test1\\"
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	print("testing folder: " + folder_name)
	var tubeBarcode = "123456789"
	print("test long newTubeBarcode: " + tubeBarcode)
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setTubeBarcode(tubeBarcode)
	TubeBarcodeChecker.checkInput()
	if (TubeBarcodeChecker.badInputFlag == true) {print("result: pass")}
	else {print("result: fail"); result = false}

	var folder_name = "test1\\"
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	print("testing folder: " + folder_name)
	var tubeBarcode = "B2345678"
	print("test long newTubeBarcode: " + tubeBarcode)
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setTubeBarcode(tubeBarcode)
	TubeBarcodeChecker.checkInput()
	if (TubeBarcodeChecker.badInputFlag == true) {print("result: pass")}
	else {print("result: fail"); result = false}

	var folder_name = "test1\\"
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	print("testing folder: " + folder_name)
	var tubeBarcode = "12345678"
	print("test with null lineArray: " + tubeBarcode)
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setTubeBarcode(tubeBarcode)
	TubeBarcodeChecker.checkInput()
	if (TubeBarcodeChecker.badInputFlag == true) {print("result: pass")}
	else {print("result: fail"); result = false}

	var folder_name = "test1\\"
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	print("testing folder: " + folder_name)
	var tubeBarcode = "12345678"
	print("test nominal sequence with non-duplicate barcode: " + tubeBarcode)
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678")
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	TubeBarcodeChecker.setTubeBarcode(tubeBarcode)
	TubeBarcodeChecker.checkInput()
	if (TubeBarcodeChecker.badInputFlag == false) {print("result: pass")}
	else {print("result: fail"); result = false}

	var folder_name = "test1\\"
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	print("testing folder: " + folder_name)
	var tubeBarcode = "P0000000"
	print("test nominal sequence with non-duplicate barcode: " + tubeBarcode)
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678")
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	TubeBarcodeChecker.setTubeBarcode(tubeBarcode)
	TubeBarcodeChecker.checkInput()
	if (TubeBarcodeChecker.badInputFlag == false) {print("result: pass")}
	else {print("result: fail"); result = false}

	var folder_name = "test1\\"
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	print("testing folder: " + folder_name)
	var tubeBarcode = "01142021"
	print("test nominal sequence with duplicate barcode of A2: " + tubeBarcode)
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678")
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	TubeBarcodeChecker.setTubeBarcode(tubeBarcode)
	TubeBarcodeChecker.checkInput()
	if (TubeBarcodeChecker.badInputFlag == true) {print("result: pass")}
	else {print("result: fail"); result = false}

	var folder_name = "test1\\"
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	print("testing folder: " + folder_name)
	var tubeBarcode = "01141936"
	print("test nominal sequence with duplicate barcode of H12: " + tubeBarcode)
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678")
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	TubeBarcodeChecker.setTubeBarcode(tubeBarcode)
	TubeBarcodeChecker.checkInput()
	if (TubeBarcodeChecker.badInputFlag == true) {print("result: pass")}
	else {print("result: fail"); result = false}

	var folder_name = "test2\\"
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	print("testing folder: " + folder_name)
	var tubeBarcode = "01141960"
	print("test nominal sequence with EMPTY at H12 and duplicate barcode at H11: " + tubeBarcode)
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678")
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	TubeBarcodeChecker.setTubeBarcode(tubeBarcode)
	TubeBarcodeChecker.checkInput()
	if (TubeBarcodeChecker.badInputFlag == true) {print("result: pass")}
	else {print("result: fail"); result = false}

	var folder_name = "test3\\"
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	print("testing folder: " + folder_name)
	var tubeBarcode = "01141980"
	print("test nominal sequence with EMPTY at C6 and duplicate barcode at D11: " + tubeBarcode)
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678")
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	TubeBarcodeChecker.setTubeBarcode(tubeBarcode)
	TubeBarcodeChecker.checkInput()
	if (TubeBarcodeChecker.badInputFlag == true) {print("result: pass")}
	else {print("result: fail"); result = false}

	if (result) {print("unit_test_checkInput(): PASS"); return true}
	else {print("unit_test_checkInput(): FAIL"); return false}
}

/************************************************
UNIT TEST for TubeBarcodeChecker.writeFile()
tests that check to make sure new file is always written properly
full test results output to VWorks log
RETURNS: BOOLEAN (true if test passes; false if test fails)
*************************************************/
function unit_test_writeFile() {
	var result = true

	FOLDER_AUTOMATED_TESTS = ROOT_FOLDER_AUTOMATED_TESTS + "unit tests for writeFile()\\"
	
	print("UNIT TESTS for TubeBarcodeChecker.writeFile()")
	print("root folder for tests: " + FOLDER_AUTOMATED_TESTS)

	var folder_name = "test1\\"
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	print("testing folder: " + folder_name)
	print("check that file is NOT written when badBarcodeFlag=null")
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.badBarcodeFlag = null

	// BEFORE writeFile()
	var path1 = FOLDER_AUTOMATED_TESTS + folder_name + "rack_12345678.csv"
	var file1 = new File()
	file1.Open(path1, 0, 0)
	var file_contents1 = file1.Read()
	file1.Close()

	TubeBarcodeChecker.writeFile()

	// AFTER writeFile()
	var path2 = FOLDER_AUTOMATED_TESTS + folder_name + "rack_12345678.csv"
	var file2 = new File()
	file2.Open(path2, 0, 0)
	var file_contents2 = file2.Read()
	file2.Close()

	if (file_contents1 == file_contents2) {print("result: pass")}
	else {print("result: fail"); result = false}

	var folder_name = "test1\\"
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	print("testing folder: " + folder_name)
	print("check that file is NOT written when badBarcodeFlag=false")
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.badBarcodeFlag = false

	// BEFORE writeFile()
	var path1 = FOLDER_AUTOMATED_TESTS + folder_name + "rack_12345678.csv"
	var file1 = new File()
	file1.Open(path1, 0, 0)
	var file_contents1 = file1.Read()
	file1.Close()

	TubeBarcodeChecker.writeFile()

	// AFTER writeFile()
	var path2 = FOLDER_AUTOMATED_TESTS + folder_name + "rack_12345678.csv"
	var file2 = new File()
	file2.Open(path2, 0, 0)
	var file_contents2 = file2.Read()
	file2.Close()

	if (file_contents1 == file_contents2) {print("result: pass")}
	else {print("result: fail"); result = false}

	var folder_name = "test1\\"
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	print("testing folder: " + folder_name)
	print("check that file is NOT written when badBarcodeFlag=true, but all other properties=null")
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.badBarcodeFlag = true

	// BEFORE writeFile()
	var path1 = FOLDER_AUTOMATED_TESTS + folder_name + "rack_12345678.csv"
	var file1 = new File()
	file1.Open(path1, 0, 0)
	var file_contents1 = file1.Read()
	file1.Close()

	TubeBarcodeChecker.writeFile()

	// AFTER writeFile()
	var path2 = FOLDER_AUTOMATED_TESTS + folder_name + "rack_12345678.csv"
	var file2 = new File()
	file2.Open(path2, 0, 0)
	var file_contents2 = file2.Read()
	file2.Close()

	if (file_contents1 == file_contents2) {print("result: pass")}
	else {print("result: fail"); result = false}

	var folder_name = "test1\\"
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	print("testing folder: " + folder_name)
	print("check that file is NOT written when badBarcodeFlag=true, but newTubeBarcode=null")
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.badBarcodeFlag = true
	TubeBarcodeChecker.fileName = "a"
	TubeBarcodeChecker.lineArray = ["a","b","c"]
	TubeBarcodeChecker.missingLineNum = 1

	// BEFORE writeFile()
	var path1 = FOLDER_AUTOMATED_TESTS + folder_name + "rack_12345678.csv"
	var file1 = new File()
	file1.Open(path1, 0, 0)
	var file_contents1 = file1.Read()
	file1.Close()

	TubeBarcodeChecker.writeFile()

	// AFTER writeFile()
	var path2 = FOLDER_AUTOMATED_TESTS + folder_name + "rack_12345678.csv"
	var file2 = new File()
	file2.Open(path2, 0, 0)
	var file_contents2 = file2.Read()
	file2.Close()

	if (file_contents1 == file_contents2) {print("result: pass")}
	else {print("result: fail"); result = false}

	var folder_name = "test1\\"
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	print("testing folder: " + folder_name)
	print("check that file is NOT written when file does not exist")
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.badBarcodeFlag = true
	TubeBarcodeChecker.fileName = "rack_123456789.csv"
	TubeBarcodeChecker.lineArray = ["a","b","c"]
	TubeBarcodeChecker.missingLineNum = 1
	TubeBarcodeChecker.newTubeBarcode = "12345678"

	// BEFORE writeFile()
	var path1 = FOLDER_AUTOMATED_TESTS + folder_name + "rack_12345678.csv"
	var file1 = new File()
	file1.Open(path1, 0, 0)
	var file_contents1 = file1.Read()
	file1.Close()

	TubeBarcodeChecker.writeFile()

	// AFTER writeFile()
	var path2 = FOLDER_AUTOMATED_TESTS + folder_name + "rack_12345678.csv"
	var file2 = new File()
	file2.Open(path2, 0, 0)
	var file_contents2 = file2.Read()
	file2.Close()

	if (file_contents1 == file_contents2) {print("result: pass")}
	else {print("result: fail"); result = false}

	// test nominal sequence!
	var folder_name = "test1\\"
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	print("testing folder: " + folder_name)
	print("check that file is correctly written with new barcode in A1")
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678")
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	TubeBarcodeChecker.setTubeBarcode("01142020")
	TubeBarcodeChecker.checkInput()
	TubeBarcodeChecker.writeFile()


	// after writeFile()
	var path1 = FOLDER_AUTOMATED_TESTS + folder_name + "rack_12345678.csv"
	var file1 = new File()
	file1.Open(path1, 0, 0)
	var file_contents1 = file1.Read()
	file1.Close()

	// control (correct file)
	var path2 = FOLDER_AUTOMATED_TESTS + folder_name + "rack_12345678 - Copy.csv"
	var file2 = new File()
	file2.Open(path2, 0, 0)
	var file_contents2 = file2.Read()
	file2.Close()

	if (file_contents1 == file_contents2) {print("result: pass")}
	else {print("result: fail"); result = false}

	print("REVERTING FILE BACK TO ORIGINAL CONFIG")
	TubeBarcodeChecker.setTubeBarcode("EMPTY")
	TubeBarcodeChecker.writeFile()

	// test nominal sequence!
	var folder_name = "test2\\"
	TubeBarcodeChecker.FOLDER_NAME = FOLDER_AUTOMATED_TESTS + folder_name
	print("testing folder: " + folder_name)
	print("check that file is correctly written with new barcode in H12")
	TubeBarcodeChecker.resetState()
	TubeBarcodeChecker.setRackBarcode("12345678")
	TubeBarcodeChecker.findLatestFile()
	TubeBarcodeChecker.readFile()
	TubeBarcodeChecker.setTubeBarcode("01141936")
	TubeBarcodeChecker.checkInput()
	TubeBarcodeChecker.writeFile()


	// after writeFile()
	var path1 = FOLDER_AUTOMATED_TESTS + folder_name + "rack_12345678.csv"
	var file1 = new File()
	file1.Open(path1, 0, 0)
	var file_contents1 = file1.Read()
	file1.Close()

	// control (correct file)
	var path2 = FOLDER_AUTOMATED_TESTS + folder_name + "rack_12345678 - Copy.csv"
	var file2 = new File()
	file2.Open(path2, 0, 0)
	var file_contents2 = file2.Read()
	file2.Close()

	if (file_contents1 == file_contents2) {print("result: pass")}
	else {print("result: fail"); result = false}

	print("REVERTING FILE BACK TO ORIGINAL CONFIG")
	TubeBarcodeChecker.setTubeBarcode("EMPTY")
	TubeBarcodeChecker.writeFile()

	if (result) {print("unit_test_writeFile(): PASS"); return true}
	else {print("unit_test_checkInput(): FAIL"); return false}
}