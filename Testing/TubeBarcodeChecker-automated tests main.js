try
{
	ROOT_FOLDER_AUTOMATED_TESTS = REPO_LOCATION + "Testing\\TubeBarcodeChecker automated test files\\"

	// need to set this variable before loading TubeBarcodeChecker.js
	barcodeFolder = ROOT_FOLDER_AUTOMATED_TESTS

	// path to version of TubeBarcodeChecker.js that you want to test!
	open(TUBEBARCODECHECKER_LOCATION)

	open(REPO_LOCATION + "Testing\\TubeBarcodeChecker-automated tests.js")
	
	var result = true
	if(!unit_test_resetState()) {result = false}
	if(!unit_test_setRackBarcode()) {result = false}
	if(!unit_test_findLatestFile()) {result = false}
	if(!unit_test_readFile()) {result = false}
	if(!unit_test_setTubeBarcode()) {result = false}
	if(!unit_test_resetTubeBarcode()) {result = false}
	if(!unit_test_checkInput()) {result = false}
	if(!unit_test_writeFile()) {result = false}

	if (result) {print("PASSED ALL UNIT TESTS")}
	else {print("FAILED UNIT TESTS")}
}
catch(e)
{
	print("Exception: " + e)
;}

