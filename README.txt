This is a source of unit tests for TubeBarcodeChecker.js. To use it:
1. Open /VWorks Protocol Files/TubeBarcodeChecker - unit tests.pro
2. Select corresponding device file if prompted
3. In the protocol, set path for REPO_LOCATION based on where you cloned this repo on your local system
4. In the protocol, set path for TUBEBARCODECHECKER_LOCATION based on where TubeBarcodeChecker.js is that you want to test
5. Run protocol! Results will print to the VWorks log.

